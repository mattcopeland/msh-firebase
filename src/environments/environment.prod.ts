export const environment = {
  production: true,
  hmr: false,
  firebase: {
    apiKey: 'AIzaSyD7_x8LTwDJoNJFeBZy_JPYdVKN4QcfROw',
    authDomain: 'material-supply-hub.firebaseapp.com',
    databaseURL: 'https://material-supply-hub.firebaseio.com',
    projectId: 'material-supply-hub',
    storageBucket: 'material-supply-hub.appspot.com',
    messagingSenderId: '966215523763'
  }
};
