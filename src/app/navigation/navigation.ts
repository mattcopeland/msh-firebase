export const navigation = [
  {
    id: 'applications',
    title: 'Applications',
    type: 'group',
    children: [
      {
        id: 'rfq',
        title: 'RFQ',
        type: 'item',
        icon: 'email',
        url: '/rfq',
        badge: {
          title: 1,
          bg: '#F44336',
          fg: '#FFFFFF'
        }
      }
    ]
  }
];
