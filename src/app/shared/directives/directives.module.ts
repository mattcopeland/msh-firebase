import { NgModule } from '@angular/core';

import { DropZoneDirective } from './drop-zone/drop-zone.directive';

@NgModule({
  declarations: [DropZoneDirective],
  imports: [],
  exports: [DropZoneDirective]
})
export class SharedDirectivesModule {}
