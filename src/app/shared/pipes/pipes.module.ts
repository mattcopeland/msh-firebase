import { NgModule } from '@angular/core';

import { FileSizePipe } from './file-size.pipe';

@NgModule({
  declarations: [FileSizePipe],
  imports: [],
  exports: [FileSizePipe]
})
export class SharedPipesModule {}
