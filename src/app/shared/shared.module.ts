import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedDirectivesModule } from './directives/directives.module';
import { SharedPipesModule } from './pipes/pipes.module';

@NgModule({
  imports: [CommonModule, SharedDirectivesModule, SharedPipesModule],
  exports: [CommonModule, SharedDirectivesModule, SharedPipesModule]
})
export class SharedModule {}
