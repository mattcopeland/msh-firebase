import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ToolbarService } from '../toolbar.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { AuthService } from 'app/core/services/auth.service';

@Component({
  selector: 'fuse-home-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class HomeToolbarComponent implements OnInit {
  constructor(
    private toolbarService: ToolbarService,
    private sidebarService: FuseSidebarService,
    public authService: AuthService
  ) {}

  ngOnInit() {}

  scrollTo(target: string): void {
    this.toolbarService.changeScrollTarget(target);
  }

  toggleSidebarOpened(key) {
    this.sidebarService.getSidebar('home-navbar').toggleOpen();
  }
}
