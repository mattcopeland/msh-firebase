import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ToolbarService {
  private scrollTarget = new Subject<string>();
  scrollTargetChanges$ = this.scrollTarget.asObservable();

  constructor() {}

  changeScrollTarget(target: string): void {
    this.scrollTarget.next(target);
  }
}
