import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatIconModule,
  MatMenuModule,
  MatProgressBarModule,
  MatToolbarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseToolbarComponent } from 'app/main/toolbar/toolbar.component';
import { FuseSearchBarModule, FuseShortcutsModule } from '@fuse/components';

import { AuthService } from 'app/core/services/auth.service';
import { HomeToolbarComponent } from './home/toolbar.component';
import { ToolbarService } from './toolbar.service';

@NgModule({
  declarations: [FuseToolbarComponent, HomeToolbarComponent],
  imports: [
    RouterModule,

    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatProgressBarModule,
    MatToolbarModule,

    FuseSharedModule,
    FuseSearchBarModule,
    FuseShortcutsModule
  ],
  providers: [AuthService, ToolbarService],
  exports: [FuseToolbarComponent, HomeToolbarComponent]
})
export class FuseToolbarModule {}
