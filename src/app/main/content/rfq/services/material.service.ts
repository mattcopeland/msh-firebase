import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { Material } from '../models/material.model';

@Injectable()
export class MaterialService {
  materialsCollection: AngularFirestoreCollection<Material>;
  materials: Observable<any[]>;

  constructor(public afs: AngularFirestore) {
    this.materials = afs.collection('materials').valueChanges();
  }

  /**
   * Get full list of all materials
   */
  getMaterials(): Observable<any> {
    return this.materials;
  }
}
