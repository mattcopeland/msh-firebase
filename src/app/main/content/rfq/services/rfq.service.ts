import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';
import * as moment from 'moment';
import { AuthService } from 'app/core/services/auth.service';

@Injectable()
export class RfqService {
  materials: Observable<any[]>;

  constructor(public afs: AngularFirestore, private authService: AuthService) {
    // this.materials = afs.collection('materials').valueChanges();
  }

  /**
   * Get full list of all materials
   */
  saveGeneralInformation(data): void {
    // Add the user's id to the RFQ
    data.submitter = this.authService.currentUser.uid;
    data.enableNegotiation = !!data.enableNegotiation;
    this.afs
      .collection('rfqs')
      .add(data)
      .then(function(docRef) {
        console.log(docRef.id);
      });
  }
}
