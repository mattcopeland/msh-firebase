import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE
} from '@angular/material/core';
import * as moment from 'moment';
import { MaterialService } from '../services/material.service';
import { Material } from '../models/material.model';
import { CurrencyService } from 'app/core/services/currency.service';
import { Subscription } from 'rxjs';
import { RfqService } from '../services/rfq.service';
import { UserService } from '../../user/user.service';
import { User, UserAddress } from 'app/main/content/user/user.model';
import { AuthService } from '../../../../core/services/auth.service';
import { Observable } from 'rxjs/Observable';
import {
  AngularFireUploadTask,
  AngularFireStorage
} from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';
import { tap, finalize } from 'rxjs/operators';

const MY_FORMATS = {
  parse: {
    dateInput: 'LL'
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'fuse-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  providers: [{ provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }]
})
export class CreateComponent implements OnInit, OnDestroy {
  materials: Material[];
  filteredMaterials: Material[];
  quoteCurrencies: string[];
  subscriptions: Subscription[] = [];

  users: User[];
  userAddresses: UserAddress[];

  materialOptions = {
    materialType: [],
    materialProcess: [],
    materialSpecification: [],
    materialDescription: []
  };

  minDate = moment();
  maxDate = moment().add(12, 'months');

  // File Upload stuff
  // Main task
  task: AngularFireUploadTask;
  // Progress monitoring
  percentage: Observable<number>;
  snapshot: Observable<any>;
  // Download URL
  downloadURL: Observable<string>;
  // State for dropzone CSS toggling
  isHovering: boolean;
  files = [];
  fileName: string;
  supportedFileTypes: Array<string> = [
    'image/jpeg',
    'application/pdf',
    'application/json'
  ];

  // Vertical Stepper
  rfqStep1: FormGroup;
  rfqStep2: FormGroup;
  verticalStepperStep3: FormGroup;
  rfqStep1Errors: any;
  rfqStep2Errors: any;
  verticalStepperStep3Errors: any;

  constructor(
    private rfqService: RfqService,
    private materialService: MaterialService,
    private currencyService: CurrencyService,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private afs: AngularFirestore
  ) {}

  ngOnInit() {
    // Get the list of currencies
    this.subscriptions.push(
      this.currencyService
        .getCurrencies()
        .subscribe(
          (data: string[]) => (this.quoteCurrencies = data),
          (err: any) => console.log(err),
          () => console.log('Get currencies done')
        )
    );

    this.subscriptions.push(
      this.userService
        .getUserAddresses(this.authService.getCurrentUser().uid)
        .subscribe(
          (data: UserAddress[]) => {
            console.log(data);
            this.userAddresses = data;
          },
          (err: any) => console.log(err),
          () => console.log('Get users done')
        )
    );

    // Form setup
    // Step 1 Form
    this.rfqStep1 = this.formBuilder.group({
      rfqName: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('[a-zA-Z].*')
        ])
      ],
      poNumber: [''],
      rfqDescription: [''],
      quoteNeededBy: [null, Validators.required],
      anticipatedAwardDate: [null, Validators.required],
      quoteCurrency: ['', Validators.required],
      paymentTerms: ['', Validators.required],
      enableNegotiation: true
    });

    // Step 2 Form
    this.rfqStep2 = this.formBuilder.group({
      partNumber: ['', Validators.pattern('[a-zA-Z].*')],
      materialType: ['', Validators.required],
      materialProcess: [{ value: '', disabled: true }, Validators.required],
      materialSpecification: [
        { value: '', disabled: true },
        Validators.required
      ],
      materialDescription: [{ value: '', disabled: true }, Validators.required]
    });

    // Watch the material type for changes and then update the processes list
    this.rfqStep2.get('materialType').valueChanges.subscribe(type => {
      console.log('type change');
      this.filteredMaterials = this.materials.filter(function(material) {
        return material.materialType === type;
      });
      this.populateMaterialOptions(this.filteredMaterials, 'materialProcess');
    });

    // Watch the material process for changes and then update the specifications list
    this.rfqStep2.get('materialProcess').valueChanges.subscribe(process => {
      console.log('process change');
      this.filteredMaterials = this.materials.filter(function(material) {
        return material.materialProcess === process;
      });
      this.populateMaterialOptions(
        this.filteredMaterials,
        'materialSpecification'
      );
    });

    // Watch the material specification for changes and then update the descriptions list
    this.rfqStep2
      .get('materialSpecification')
      .valueChanges.subscribe(specification => {
        console.log('spec change');
        this.filteredMaterials = this.materials.filter(function(material) {
          return material.materialSpecification === specification;
        });
        this.populateMaterialOptions(
          this.filteredMaterials,
          'materialDescription'
        );
      });

    this.verticalStepperStep3 = this.formBuilder.group({
      city: ['', Validators.required],
      state: ['', Validators.required],
      postalCode: ['', [Validators.required, Validators.maxLength(5)]]
    });
  }

  saveStep1(formValues) {
    if (this.rfqStep1.valid) {
      console.log(formValues);
      formValues.anticipatedAwardDate = moment(
        formValues.anticipatedAwardDate
      ).toDate();
      formValues.quoteNeededBy = moment(formValues.quoteNeededBy).toDate();
      formValues.files = this.files;
      this.rfqService.saveGeneralInformation(formValues);
      // Get all the materials
      this.subscriptions.push(
        this.materialService.getMaterials().subscribe(
          (data: Material[]) => {
            console.log(data);
            this.materials = data;
            // Create an array of unique material types
            this.materialOptions.materialType = [];
            data.forEach(material => {
              if (
                !this.materialOptions.materialType.includes(
                  material.materialType
                )
              ) {
                this.materialOptions.materialType.push(material.materialType);
              }
            });
          },
          (err: any) => console.log(err),
          () => console.log('Get materials done')
        )
      );
    }
  }

  populateMaterialOptions(filteredMaterials, name) {
    const options = [];
    filteredMaterials.forEach(material => {
      if (!options.includes(material[name])) {
        options.push(material[name]);
      }
    });
    this.materialOptions[name] = options;
    this.resetMaterialOptions(name);
  }

  resetMaterialOptions(name) {
    const options = Object.keys(this.materialOptions);
    const index = options.findIndex(option => option === name);
    for (let i = index; i < options.length; i++) {
      if (!this.rfqStep2.get(options[i]).pristine) {
        this.rfqStep2.get(options[i]).reset();
      }
      if (i === index) {
        if (this.rfqStep2.get(options[i]).disabled) {
          this.rfqStep2.get(options[i]).enable();
        }
      } else {
        if (this.rfqStep2.get(options[i]).enabled) {
          this.rfqStep2.get(options[i]).disable();
        }
      }
    }
  }

  finishVerticalStepper() {
    alert('You have finished the vertical stepper!');
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  startUpload(event: FileList) {
    // The File object
    const file = event.item(0);
    this.fileName = file.name;

    // Client-side validation example
    const allowedFileType = this.supportedFileTypes.includes(file.type);

    if (!allowedFileType) {
      console.log(file.type.split('/'));
      console.error('unsupported file type :( ');
      return;
    }

    // The storage path
    const path = `rfq-docs/${new Date().getTime()}_${file.name}`;

    const fileRef = this.storage.ref(path);

    // Totally optional metadata
    const customMetadata = { app: 'RFQ' };

    // The main task
    this.task = this.storage.upload(path, file, { customMetadata });

    // Progress monitoring
    this.percentage = this.task.percentageChanges();
    this.snapshot = this.task.snapshotChanges().pipe(
      tap(snap => {
        if (snap.bytesTransferred === snap.totalBytes) {
          // Update firestore on completion
          this.files.push({
            name: this.fileName,
            path,
            size: snap.totalBytes
          });
        }
      }),
      finalize(() => {
        this.fileName = null;
        this.downloadURL = fileRef.getDownloadURL();
      })
    );
  }

  // Determines if the upload task is active
  isActive(snapshot) {
    return (
      snapshot.state === 'running' &&
      snapshot.bytesTransferred < snapshot.totalBytes
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
