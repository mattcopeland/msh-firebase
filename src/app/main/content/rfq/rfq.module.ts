import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatStepperModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatListModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { FuseSharedModule } from '@fuse/shared.module';
import { RfqComponent } from './rfq.component';
import { CreateComponent } from './create/create.component';
import { AuthGaurd } from 'app/core/route-gaurds/auth-gaurd.service';
import { MaterialService } from 'app/main/content/rfq/services/material.service';
import { RfqService } from 'app/main/content/rfq/services/rfq.service';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { SharedModule } from 'app/shared/shared.module';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faFileImage } from '@fortawesome/free-solid-svg-icons';
library.add(faFileImage);

const routes = [
  {
    path: '',
    component: RfqComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: 'create',
    component: CreateComponent,
    canActivate: [AuthGaurd]
  },
  {
    path: 'upload',
    component: FileUploadComponent,
    canActivate: [AuthGaurd]
  }
];

@NgModule({
  declarations: [RfqComponent, CreateComponent, FileUploadComponent],
  imports: [
    RouterModule.forChild(routes),

    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatListModule,

    FontAwesomeModule,

    FuseSharedModule,
    SharedModule
  ],
  providers: [AuthGaurd, MaterialService, RfqService],
  exports: [RfqComponent]
})
export class RfqModule {}
