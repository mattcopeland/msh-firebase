export interface Material {
  materialType: string;
  materialProcess: string;
  materialSpecification: string;
  materialDescription: string;
  materialWeight: number;
}
