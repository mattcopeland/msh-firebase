export interface User {
  uid: string;
  email?: string | null;
  photoURL?: string;
  displayName?: string;
  firstName?: string;
  lastName?: string;
  roles?: Role;
}

export interface Role {
  subscriber?: boolean;
  admin?: boolean;
}

export interface UserAddress {
  address1: string;
  address2?: string;
  city: string;
  state: string;
  zip: string;
}
