import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatStepperModule,
  MatCheckboxModule,
  MatDatepickerModule
} from '@angular/material';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { FuseSharedModule } from '@fuse/shared.module';
import { AuthGaurd } from 'app/core/route-gaurds/auth-gaurd.service';
import { ProfileComponent } from './profile/profile.component';

const routes = [
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGaurd]
  }
];

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    RouterModule.forChild(routes),

    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatStepperModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatMomentDateModule,

    FuseSharedModule
  ],
  providers: [AuthGaurd],
  exports: []
})
export class UserModule {}
