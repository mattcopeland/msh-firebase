import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { User, UserAddress } from 'app/main/content/user/user.model';
import { AuthService } from '../../../core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  usersCollection: AngularFirestoreCollection<User>;
  users: Observable<User[]>;

  userAddressesCollection: AngularFirestoreCollection<UserAddress>;
  userAddresses: Observable<UserAddress[]>;

  constructor(public afs: AngularFirestore, private authService: AuthService) {
    this.usersCollection = this.afs.collection('users');
    this.users = this.usersCollection.valueChanges();
  }

  /**
   * Get full list of all materials
   */
  getUsers(): Observable<User[]> {
    return this.users;
  }

  getUserAddresses(uid: string): Observable<UserAddress[]> {
    this.userAddressesCollection = this.afs.collection('userAddresses', ref =>
      ref.where('uid', '==', uid).orderBy('edited')
    );
    this.userAddresses = this.userAddressesCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as UserAddress;
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    return this.userAddresses;
  }
}
