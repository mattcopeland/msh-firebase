import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../../../../core/services/auth.service';

@Component({
  selector: 'fuse-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  animations: fuseAnimations
})
export class FuseResetPasswordComponent implements OnInit {
  resetPasswordForm: FormGroup;
  resetPasswordFormErrors: any;
  actionCode: string = this.route.snapshot.queryParamMap.get('oobCode');
  private userEmail: string;

  constructor(
    private fuseConfig: FuseConfigService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private afAuth: AngularFireAuth,
    private authService: AuthService
  ) {
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'none',
        toolbar: 'none',
        footer: 'none'
      }
    });

    this.resetPasswordFormErrors = {
      email: {},
      password: {},
      passwordConfirm: {}
    };
  }

  ngOnInit() {
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      passwordConfirm: ['', [Validators.required, confirmPassword]]
    });

    this.resetPasswordForm.valueChanges.subscribe(() => {
      this.onResetPasswordFormValuesChanged();
    });

    // Check for password reset code or redirect to forgot password page
    if (this.actionCode) {
      this.verifyCode(this.actionCode);
    } else {
      this.router.navigate(['/auth/forgot-password']);
    }
  }

  /**
   *
   * @param code Verify the user's password reset code is valid
   */
  verifyCode(code) {
    this.afAuth.auth
      .verifyPasswordResetCode(code)
      .then(email => {
        this.resetPasswordForm.controls['email'].patchValue(email);
        this.userEmail = email;
      })
      .catch(error => this.router.navigate(['/auth/forgot-password']));
  }

  resetPassword() {
    this.authService.resetPassword(
      this.actionCode,
      this.userEmail,
      this.resetPasswordForm.get('password').value
    );
    // .then(() => {
    //   this.authService.loginUser(
    //     this.resetPasswordForm.get('email').value,
    //     this.resetPasswordForm.get('password').value
    //   );
    //   // .subscribe(data => {
    //   //   // if (data.user) {
    //   //   this.router.navigate(['/rfq']);
    //   //   // }
    //   // });
    // })
    // .catch(error => console.log(error));
  }

  onResetPasswordFormValuesChanged() {
    for (const field in this.resetPasswordFormErrors) {
      if (!this.resetPasswordFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.resetPasswordFormErrors[field] = {};

      // Get the control
      const control = this.resetPasswordForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.resetPasswordFormErrors[field] = control.errors;
      }
    }
  }
}

function confirmPassword(control: AbstractControl) {
  if (!control.parent || !control) {
    return;
  }

  const password = control.parent.get('password');
  const passwordConfirm = control.parent.get('passwordConfirm');

  if (!password || !passwordConfirm) {
    return;
  }

  if (passwordConfirm.value === '') {
    return;
  }

  if (password.value !== passwordConfirm.value) {
    return {
      passwordsNotMatch: true
    };
  }
}
