import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AngularFireAuth } from 'angularfire2/auth';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Subscription } from 'rxjs';

@Component({
  selector: 'fuse-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  animations: fuseAnimations
})
export class FuseForgotPasswordComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  forgotPasswordForm: FormGroup;
  forgotPasswordFormErrors: any;
  snackBarRef: MatSnackBarRef<SimpleSnackBar>;
  emailSent: boolean;

  constructor(
    private fuseConfig: FuseConfigService,
    private formBuilder: FormBuilder,
    private afAuth: AngularFireAuth,
    public snackBar: MatSnackBar
  ) {
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'none',
        toolbar: 'none',
        footer: 'none'
      }
    });

    this.forgotPasswordFormErrors = {
      email: {}
    };
  }

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });

    this.subscriptions.push(
      this.forgotPasswordForm.valueChanges.subscribe(() => {
        this.onForgotPasswordFormValuesChanged();
      })
    );
  }

  sendResetPasswordEmail() {
    this.afAuth.auth
      .sendPasswordResetEmail(this.forgotPasswordForm.value.email)
      .then(() => (this.emailSent = true))
      .catch(error => {
        this.snackBarRef = this.snackBar.open(
          "That email doesn't exist, Please try again.",
          null,
          {
            duration: 5000,
            panelClass: 'error'
          }
        );
      });
  }

  onForgotPasswordFormValuesChanged() {
    // Dismiss the snack bar error
    if (this.snackBarRef) {
      this.snackBarRef.dismiss();
    }

    for (const field in this.forgotPasswordFormErrors) {
      if (!this.forgotPasswordFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.forgotPasswordFormErrors[field] = {};

      // Get the control
      const control = this.forgotPasswordForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.forgotPasswordFormErrors[field] = control.errors;
      }
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
