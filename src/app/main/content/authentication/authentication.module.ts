import { NgModule } from '@angular/core';

import { LoginModule } from './login/login.module';
import { RegisterModule } from './register/register.module';
import { ForgotPasswordModule } from './forgot-password/forgot-password.module';
import { ResetPasswordModule } from './reset-password/reset-password.module';
import { MailConfirmModule } from './mail-confirm/mail-confirm.module';
import { AccountManagementModule } from './account-management/account-management.module';
import { AccountManagementGaurd } from './account-management/route-gaurds/account-management.service';

@NgModule({
  imports: [
    // Auth
    LoginModule,
    RegisterModule,
    ForgotPasswordModule,
    ResetPasswordModule,
    MailConfirmModule,
    AccountManagementModule
  ],
  providers: [AccountManagementGaurd]
})
export class AuthModule {}
