import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/core/services/auth.service';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'fuse-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: fuseAnimations
})
export class FuseLoginComponent implements OnInit {
  loginForm: FormGroup;
  loginFormErrors: any;
  snackBarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(
    private fuseConfig: FuseConfigService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    public snackBar: MatSnackBar,
    private router: Router
  ) {
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'none',
        toolbar: 'none',
        footer: 'none'
      }
    });

    this.loginFormErrors = {
      email: {},
      password: {}
    };
  }

  ngOnInit() {
    // Initiallize the form
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });

    // Watch for changes to the login form values
    this.loginForm.valueChanges.subscribe(() => {
      this.onLoginFormValuesChanged();
    });
  }

  login() {
    this.authService.loginUser(
      this.loginForm.get('email').value,
      this.loginForm.get('password').value
    );
    // .subscribe(user => {
    //   if (user) {
    //     if (!user.emailVerified) {
    //       this.router.navigate(['/auth/mail-confirm']);
    //     } else if (this.authService.redirectUrl) {
    //       this.router.navigateByUrl(this.authService.redirectUrl);
    //     } else {
    //       this.router.navigate(['/rfq']);
    //     }
    //   } else {
    //     this.snackBarRef = this.snackBar.open(
    //       'Incorrect email / password combination',
    //       null,
    //       {
    //         duration: 5000,
    //         panelClass: 'error'
    //       }
    //     );
    //   }
    // });
  }

  onLoginFormValuesChanged() {
    // Dismiss the snack bar error
    if (this.snackBarRef) {
      this.snackBarRef.dismiss();
    }
    // Reset the errors on the fields
    for (const field in this.loginFormErrors) {
      if (!this.loginFormErrors.hasOwnProperty(field)) {
        continue;
      }

      // Clear previous errors
      this.loginFormErrors[field] = {};

      // Get the control
      const control = this.loginForm.get(field);

      if (control && control.dirty && !control.valid) {
        this.loginFormErrors[field] = control.errors;
      }
    }
  }
}
