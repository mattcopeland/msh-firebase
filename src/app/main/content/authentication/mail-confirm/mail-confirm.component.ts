import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/core/services/auth.service';
import { Subscription } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'fuse-mail-confirm',
  templateUrl: './mail-confirm.component.html',
  styleUrls: ['./mail-confirm.component.scss'],
  animations: fuseAnimations
})
export class FuseMailConfirmComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  isVerified: Boolean;
  mode: String;
  actionCode: String;

  constructor(
    private fuseConfig: FuseConfigService,
    public authService: AuthService,
    private route: ActivatedRoute,
    private afAuth: AngularFireAuth
  ) {
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'none',
        toolbar: 'none',
        footer: 'none'
      }
    });
  }

  ngOnInit() {
    this.subscriptions.push(
      this.authService.isVerified.subscribe(verified => {
        this.isVerified = verified;
      })
    );

    this.subscriptions.push(
      this.route.queryParams.subscribe(params => {
        this.mode = params['mode'];
        this.actionCode = params['oobCode'];
        if (this.mode === 'verifyEmail') {
          this.handleVerifyEmail(this.actionCode);
        }
      })
    );
  }

  handleVerifyEmail(actionCode) {
    this.afAuth.auth
      .applyActionCode(actionCode)
      .then(() => (this.isVerified = true))
      .catch(error => console.log(error));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
