import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

@Injectable()
export class AccountManagementGaurd implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const mode = route.queryParamMap.get('mode');
    const actionCode = route.queryParamMap.get('oobCode');

    switch (mode) {
      case 'verifyEmail': {
        this.router.navigate(['/auth/mail-confirm'], {
          queryParams: {
            mode: mode,
            oobCode: actionCode
          }
        });
        break;
      }
      case 'resetPassword': {
        this.router.navigate(['/auth/reset-password'], {
          queryParams: {
            mode: mode,
            oobCode: actionCode
          }
        });
        break;
      }
      default: {
        this.router.navigate(['/']);
        return false;
      }
    }
  }
}
