import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

import { AccountManagementComponent } from './account-management.component';
import { AccountManagementGaurd } from './route-gaurds/account-management.service';

const routes = [
  {
    path: 'account-management',
    component: AccountManagementComponent,
    canActivate: [AccountManagementGaurd]
  }
];

@NgModule({
  declarations: [AccountManagementComponent],
  imports: [RouterModule.forChild(routes), FuseSharedModule]
})
export class AccountManagementModule {}
