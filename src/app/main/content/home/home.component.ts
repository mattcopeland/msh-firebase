import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FuseConfigService } from '@fuse/services/config.service';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { ToolbarService } from '../../toolbar/toolbar.service';
import { Subscription } from 'rxjs';
import { HomeService } from './home.service';

@Component({
  selector: 'fuse-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  @ViewChild(FusePerfectScrollbarDirective)
  directiveScroll: FusePerfectScrollbarDirective;

  subscriptions: Subscription[] = [];
  formGroup: FormGroup;
  formSubmitted: Boolean = false;

  constructor(
    private fuseConfig: FuseConfigService,
    private toolbarService: ToolbarService,
    private homeService: HomeService,
    formBuilder: FormBuilder
  ) {
    this.fuseConfig.setConfig({
      layout: {
        navigation: 'homepage',
        toolbar: 'homepage',
        footer: 'none'
      }
    });

    this.formGroup = formBuilder.group({
      requestDemo: '',
      waitList: '',
      contact: '',
      person_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ''
    });
  }

  ngOnInit() {
    this.subscriptions.push(
      this.toolbarService.scrollTargetChanges$.subscribe(scrollTarget => {
        this.directiveScroll.scrollToY(
          document.getElementById(scrollTarget).offsetTop,
          400,
          document.getElementById(scrollTarget)
        );
      })
    );
  }

  scrollTo(target: string, action: string): void {
    if (action) {
      this.formGroup.controls[action].patchValue(true);
    }
    this.toolbarService.changeScrollTarget(target);
  }

  onFormSubmit(formValues: any) {
    if (this.formGroup.valid) {
      this.homeService.saveContactInformation(formValues);
      this.formSubmitted = true;
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
