import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  materials: Observable<any[]>;

  constructor(public afs: AngularFirestore) {}

  /**
   * Save contact info
   */
  saveContactInformation(data): void {
    this.afs.collection('homepage-contact').add(data);
  }
}
