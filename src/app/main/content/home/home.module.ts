import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
  MatButtonModule,
  MatToolbarModule,
  MatIconModule,
  MatSlideToggleModule,
  MatFormFieldModule,
  MatInputModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { HomeComponent } from './home.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faBalanceScale,
  faCalendarAlt,
  faEdit
} from '@fortawesome/free-solid-svg-icons';
library.add(faBalanceScale, faCalendarAlt, faEdit);

const routes = [
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FuseSharedModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    FontAwesomeModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule {}
