import { Component, OnInit } from '@angular/core';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { ToolbarService } from 'app/main/toolbar/toolbar.service';
import { AuthService } from 'app/core/services/auth.service';

@Component({
  selector: 'fuse-home-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class HomeNavbarComponent implements OnInit {
  constructor(
    private toolbarService: ToolbarService,
    private sidebarService: FuseSidebarService,
    public authService: AuthService
  ) {}

  ngOnInit() {}

  scrollTo(target: string): void {
    this.toolbarService.changeScrollTarget(target);
    this.toggleSidebarOpened();
  }

  toggleSidebarOpened() {
    this.sidebarService.getSidebar('home-navbar').toggleOpen();
  }
}
