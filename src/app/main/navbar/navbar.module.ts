import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MatButtonModule, MatIconModule } from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseNavbarComponent } from 'app/main/navbar/navbar.component';
import { FuseNavigationModule } from '@fuse/components';
import { HomeNavbarComponent } from 'app/main/navbar/home/navbar.component';

@NgModule({
  declarations: [FuseNavbarComponent, HomeNavbarComponent],
  imports: [
    RouterModule,

    MatButtonModule,
    MatIconModule,

    FuseSharedModule,
    FuseNavigationModule
  ],
  exports: [FuseNavbarComponent, HomeNavbarComponent]
})
export class FuseNavbarModule {}
