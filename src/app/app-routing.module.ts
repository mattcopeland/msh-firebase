import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'auth',
    loadChildren:
      './main/content/authentication/authentication.module#AuthModule'
  },
  {
    path: 'rfq',
    loadChildren: './main/content/rfq/rfq.module#RfqModule'
  },
  {
    path: '',
    loadChildren: './main/content/home/home.module#HomeModule'
  },
  {
    path: 'user',
    loadChildren: './main/content/user/user.module#UserModule'
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
