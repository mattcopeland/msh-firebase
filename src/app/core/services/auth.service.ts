import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument
} from 'angularfire2/firestore';
import { User } from 'app/main/content/user/user.model';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Observable, of, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class AuthService {
  user: Observable<User | null>;
  currentUser: User | null;
  redirectUrl: string;
  isVerified: Observable<Boolean>;
  snackBarRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(
    private afs: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router,
    public snackBar: MatSnackBar
  ) {
    this.redirectUrl = '';

    this.isVerified = this.afAuth.authState.pipe(
      switchMap(user => {
        return of(user && user.emailVerified);
      })
    );

    // get auth data and then get firestore user document || null
    this.user = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
    this.setCurrentUser();
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  registerUser(registerData: any) {
    this.afAuth.auth
      .createUserWithEmailAndPassword(registerData.email, registerData.password)
      .then(credential => {
        credential.user.sendEmailVerification();
        this.updateUserData(credential.user, registerData);
        this.router.navigate(['/rfq']);
      })
      .catch(error => this.handleError(error));
  }

  // Sets user data to firestore after succesful login
  private updateUserData(user: User, registerData: any) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(
      `users/${user.uid}`
    );

    const data: User = {
      uid: user.uid,
      email: user.email,
      firstName: registerData.firstName,
      lastName: registerData.lastName,
      roles: {
        subscriber: true
      }
    };
    return userRef.set(data, { merge: true });
  }

  // If error, console log and notify user
  private handleError(error: Error) {
    console.error(error);
    this.snackBarRef = this.snackBar.open(error.message, null, {
      duration: 5000,
      panelClass: 'error'
    });
    return error;
  }

  loginUser(email: string, password: string) {
    // Dismiss the snack bar error
    if (this.snackBarRef) {
      this.snackBarRef.dismiss();
    }
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(data => {
        if (!data.user.emailVerified) {
          this.router.navigate(['/auth/mail-confirm']);
        } else if (this.redirectUrl) {
          this.router.navigateByUrl(this.redirectUrl);
        } else {
          this.router.navigate(['/rfq']);
        }
      })
      .catch(error => this.handleError(error));
  }

  resetPassword(actionCode: string, email: string, newPassword: string) {
    this.afAuth.auth
      .confirmPasswordReset(actionCode, newPassword)
      .then(response => {
        this.loginUser(email, newPassword);
      })
      .catch(error => console.log(error));
  }

  getCurrentUser() {
    return this.currentUser;
  }

  private setCurrentUser(): void {
    this.user.subscribe(user => (this.currentUser = user));
  }
}
