import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  AngularFirestoreDocument
} from 'angularfire2/firestore';

@Injectable()
export class CurrencyService {
  currenciesCollection: AngularFirestoreCollection<any>;
  currencies: Observable<any[]>;

  constructor(public afs: AngularFirestore) {
    this.currencies = afs.collection('currencies').valueChanges();
  }

  /**
   * Get full list of all currencies
   */
  getCurrencies(): Observable<any> {
    return this.currencies;
  }
}
