import { NgModule } from '@angular/core';
import { CurrencyService } from './services/currency.service';
import { AuthService } from './services/auth.service';
import { AuthGaurd } from './route-gaurds/auth-gaurd.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CacheInterceptor } from './interceptors/cache.interceptor';
import { MatSnackBarModule } from '@angular/material';

@NgModule({
  declarations: [],
  imports: [MatSnackBarModule],
  providers: [
    CurrencyService,
    AuthService,
    AuthGaurd,
    { provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true }
  ],
  exports: []
})
export class CoreModule {}
